//Import thu vien express
const express = require("express");
//Khai bao middleware
const voucherMiddleware = require("../middlewares/voucherMiddleware");
//import controller
const { getAllVoucher, createVoucher, getVoucherById, updateVoucher, deleteVoucher } = require("../controllers/voucherController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL voucher: ", req.url);

    next();
});
//get all drink
router.get("/vouchers", voucherMiddleware.getAllVoucherMiddleware,getAllVoucher )
//create drink
router.post("/vouchers", voucherMiddleware.postVoucherMiddleware, createVoucher);
//get a drink
router.get("/vouchers/:voucherId", voucherMiddleware.getAVoucherMiddleware, getVoucherById);
//update drink
router.put("/vouchers/:voucherId", voucherMiddleware.putVoucherMiddleware, updateVoucher);
//delete drink
router.delete("/vouchers/:voucherId", voucherMiddleware.deleteVoucherMiddleware, deleteVoucher);

module.exports = router;