//Import thu vien express
const express = require("express");
//Khai bao middleware
const diceHistoryMiddleware = require("../middlewares/diceHistoryMiddleware");
//import controller
const { getAllDiceHistory, createDice, getDiceById, updateDice, deleteDice } = require("../controllers/diceHistoryController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL dice history: ", req.url);

    next();
});
//get all drink
router.get("/dice-histories", diceHistoryMiddleware.getAllDiceHistoryMiddleware, getAllDiceHistory)
//create drink
router.post("/dice-histories", diceHistoryMiddleware.postDiceHistoryMiddleware, createDice);
//get a drink
router.get("/dice-histories/:diceHistoryId", diceHistoryMiddleware.getADiceHistoryMiddleware, getDiceById);
//update drink
router.put("/dice-histories/:diceHistoryId", diceHistoryMiddleware.putDiceHistoryMiddleware, updateDice);
//delete drink
router.delete("/dice-histories/:diceHistoryId", diceHistoryMiddleware.deleteDiceHistoryMiddleware, deleteDice);

module.exports = router;