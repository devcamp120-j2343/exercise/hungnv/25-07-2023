//import thư viện mongoose
const mongoose = require('mongoose');

// import User model
const voucherModel = require('../models/voucherModel');

//get all prize
const getAllVoucher = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    voucherModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all voucher sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//get one voucher
const getVoucherById = (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    voucherModel.findById(voucherId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get voucher by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a voucher
const createVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    const { code, discount,note } = req.body;

    //B2: kiểm tra dữ liệu
    if (!code) {
        return res.status(400).json({
            status: "Bad request",
            message: "code is required!"
        })
    }
    if (!note ) {    
        return res.status(400).json({
            status: "Bad request",
            message: "note is required!"
        })
    }
    if (typeof discount !== "number" || discount < 0 || discount > 100) {
        return res.status(400).json({
            status: "Bad request",
            message: "discount must be a number between 0 and 100"
        });
    }

    //B3: thực hiện thao tác model
    let newVoucher = {
        _id: new mongoose.Types.ObjectId(),
        code,
        discount,
        note
    }

    voucherModel.create(newVoucher)
        .then((data) => {
            return res.status(201).json({
                status: "Create new voucher sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update voucher
const updateVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    var voucherId = req.params.voucherId;
    const { code, discount,note } = req.body;

    //B2: kiểm tra dữ liệu
    if (!code) {
        return res.status(400).json({
            status: "Bad request",
            message: "code is required!"
        })
    }
    if (!note ) {    
        return res.status(400).json({
            status: "Bad request",
            message: "note is required!"
        })
    }
    if (typeof discount !== "number" || discount < 0 || discount > 100) {
        return res.status(400).json({
            status: "Bad request",
            message: "discount must be a number between 0 and 100"
        });
    }

    //B3: thực thi model
    let updateVoucher = {
        code,
        discount,
        note
    }

    voucherModel.findByIdAndUpdate(voucherId, updateVoucher)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update voucher sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//delete voucher
const deleteVoucher = (req, res) => {
    //B1: Thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    voucherModel.findByIdAndDelete(voucherId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete voucher sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//Export thanh module
module.exports = {
    getAllVoucher,
    getVoucherById,
    createVoucher,
    updateVoucher,
    deleteVoucher
}