//import thư viện mongoose
const mongoose = require('mongoose');

// import User model
const diceHistoryModel = require('../models/diceHistoryModel');

//get all dice
const getAllDiceHistory = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    diceHistoryModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all dice sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any dice",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//get one dice
const getDiceById = (req, res) => {
    //B1: thu thập dữ liệu
    var diceId = req.params.diceId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    diceHistoryModel.findById(diceId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get dice by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any dice",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a user
const createDice = (req, res) => {
    //B1: thu thập dữ liệu
    const { user } = req.body;
    const dice = Math.floor(Math.random() * 6) + 1;
    //B2: kiểm tra dữ liệu
    if (typeof dice !== 'number') {
        return res.status(400).json({
            status: "Bad request",
            message: "Dice value must be a number!"
        });
    }


    //B3: thực hiện thao tác model
    let newDiceHistory = {
        _id: new mongoose.Types.ObjectId(),
        user,
        dice
    }

    diceHistoryModel.create(newDiceHistory)
        .then((data) => {
            return res.status(201).json({
                status: "Create new dice sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update dice
const updateDice = (req, res) => {
    //B1: thu thập dữ liệu
    var diceId = req.params.diceId;
    //B1: thu thập dữ liệu
    const { user } = req.body;
    const dice = Math.floor(Math.random() * 6) + 1;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (typeof dice !== 'number') {
        return res.status(400).json({
            status: "Bad request",
            message: "Dice value must be a number!"
        });
    }

    //B3: thực thi model
    let updateDice = {
        user,
        dice

    }

    diceHistoryModel.findByIdAndUpdate(diceId, updateDice)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update dice sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any dice",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//delete drink
const deleteDice = (req, res) => {
    //B1: Thu thập dữ liệu
    var diceId = req.params.diceId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    diceHistoryModel.findByIdAndDelete(diceId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete dice sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any dice",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//Export thanh module
module.exports = {
    getAllDiceHistory,
    getDiceById,
    createDice,
    updateDice,
    deleteDice
}
