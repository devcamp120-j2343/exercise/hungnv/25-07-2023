const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const prizeSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    description: {
        type: String,
        required:false
    },
}, { timestamps: true });


module.exports = mongoose.model('prize', prizeSchema);