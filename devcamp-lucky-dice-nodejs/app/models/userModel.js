//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    username: {
        type: String,
        unique: true,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});
//b4 export module
module.exports = mongoose.model('user', userSchema);