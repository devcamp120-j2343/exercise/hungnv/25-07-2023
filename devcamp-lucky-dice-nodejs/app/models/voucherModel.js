const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const voucherSchema = new mongoose.Schema({
  code: {
    type: String,
    unique: true,
    required: true,
  },
  discount: {
    type: Number,
    required: true,
  },
  note: {
    type: String,
    required:false
  },
}, { timestamps: true });

module.exports = mongoose.model('voucher', voucherSchema);
