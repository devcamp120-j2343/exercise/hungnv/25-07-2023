const getAllDiceHistoryMiddleware = (req, res, next) => {
    console.log("Get All DiceHistory!");
    next();
}

const getADiceHistoryMiddleware = (req, res, next) => {
    console.log("Get a DiceHistory!");
    next();
}
const postDiceHistoryMiddleware = (req, res, next) => {
    console.log("Create a DiceHistory!");
    next();
}

const putDiceHistoryMiddleware = (req, res, next) => {
    console.log("Update a DiceHistory!");
    next();
}
const deleteDiceHistoryMiddleware = (req, res, next) => {
    console.log("Delete a DiceHistory!");
    next();
}

//export
module.exports = {
    getAllDiceHistoryMiddleware,
    getADiceHistoryMiddleware,
    postDiceHistoryMiddleware,
    putDiceHistoryMiddleware,
    deleteDiceHistoryMiddleware
}