const getAllPrizeMiddleware = (req, res, next) => {
    console.log("Get All Prize!");
    next();
}

const getAPrizeMiddleware = (req, res, next) => {
    console.log("Get a Prize!");
    next();
}
const postPrizeMiddleware = (req, res, next) => {
    console.log("Create a Prize!");
    next();
}

const putPrizeMiddleware = (req, res, next) => {
    console.log("Update a Prize!");
    next();
}
const deletePrizeMiddleware = (req, res, next) => {
    console.log("Delete a Prize!");
    next();
}

//export
module.exports = {
    getAllPrizeMiddleware,
    getAPrizeMiddleware,
    postPrizeMiddleware,
    putPrizeMiddleware,
    deletePrizeMiddleware
}