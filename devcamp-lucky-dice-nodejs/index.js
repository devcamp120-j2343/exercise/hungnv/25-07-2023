// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Import mongooseJS
const mongoose = require("mongoose");
// Import module path
const path = require("path");
//khai báo các model
const userModel = require('./app/models/userModel');
const diceHistorytModel = require('./app/models/diceHistoryModel');
const prizeModel = require('./app/models/prizeModel');
const vouchertModel = require('./app/models/voucherModel');
// Import router
const userRouter = require("./app/routers/userRouter");
const diceHistoryRouter = require("./app/routers/diceHistoryRouter");
const prizeRouter = require("./app/routers/prizeRouter");
const voucherRouter = require("./app/routers/voucherRouter");
// Khởi tạo app express
const app = express();
// Kết nối với MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDice")
    .then(() => {
        console.log('Successfully connected to MongooseDB');
    })
    .catch((error) => {
        throw error;
    });
// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))
// Khai báo cổng của project
const port = 8000;

// Middleware in ra thời gian hiện tại mỗi lần chạy
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
    (request, response, next) => {
        console.log("Request method: ", request.method);
        next();
    }
)
// API trả về giá trị yêu cầu
app.get('/lucky-dice', (req, res) => {
    // Tạo số ngẫu nhiên từ 1 đến 6
    const luckyNumber = Math.floor(Math.random() * 6) + 1;
    res.json({
        luckyNumber: luckyNumber
    });
});
//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/app/views"));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/lucky-dice.html"));
})

app.use("/api", userRouter);
app.use("/api", diceHistoryRouter);
app.use("/api", prizeRouter);
app.use("/api", voucherRouter);

// Khởi động server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});